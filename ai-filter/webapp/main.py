from flask import *
import random,os
import glob
from werkzeug import secure_filename


UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['txt','png','jpg','mp3'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



@app.route("/")
def index():
    name = "aaaaa"
    return render_template('index.html', name3=name)

@app.route("/randomValue")
def randomValue():
    
    try:
        empDict = {
        'ses1': random.randint(1,100),
        'ses2': random.randint(1,100),
        'ses3': random.randint(1,100)}
        jsonStr = json.dumps(empDict)

    except Exception ,e:
        print str(e)
    response = Response(jsonStr,content_type="application/json; charset=utf-8" )
    return response


@app.route('/upload',methods = ['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('index'))
    return "File Type Error."

@app.route('/uploadSong')
def uploadSong():
    return render_template('uploadSong.html')

@app.route('/songList')
def songList():
    file = glob.glob("./uploads/*")
    file = str(file) 
    return " " + str(file[1:-1])

@app.route('/selectedFile/<string:selected_file>', methods=['GET'])
def selectedFile(selected_file):

    message = './uploads/:' + selected_file
    '''
        JSON olarak, ses tipleri ve yuzdelikler donderilecek
    '''
    try:
        empDict = {
        'ses1': random.randint(1,100),
        'ses2': random.randint(1,100),
        'ses3': random.randint(1,100)}
        jsonStr = json.dumps(empDict)

    except Exception ,e:
        print str(e)
    response = Response(jsonStr,content_type="application/json; charset=utf-8" )
    return response


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
